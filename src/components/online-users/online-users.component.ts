import { Component,OnInit } from '@angular/core';
import { DataService } from '../../providers/data/data.service';
import { FirebaseListObservable } from 'angularfire2/database-deprecated';
import { Profile } from '../../models/profile/profile.interface';
import { NavController } from 'ionic-angular/navigation/nav-controller';

/**
 * Generated class for the OnlineUsersComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'app-online-users',
  templateUrl: 'online-users.component.html'
})
export class OnlineUsersComponent implements OnInit{

  userList:FirebaseListObservable<Profile[]>;

  constructor(private navCtrl: NavController,private data:DataService){

  }

  ngOnInit(){
    this.setUserOnline();
    this.getOnlineUsers();
  }

  setUserOnline(){
    //Get the authenticated user
    this.data.getAuthenticatedUserProfile().subscribe(profile =>{
      //Call to a service that sets the user online within Firebase
      this.data.setUserOnline(profile);
    })
  }

  getOnlineUsers(){
    this.userList = this.data.getOnlineUsers();
  }

  openChat(profile: Profile){
    this.navCtrl.push("MessagePage",{ profile })
  }

}

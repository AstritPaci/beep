import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Account } from '../../models/account/account.interface';
import { LoginResponse } from '../../models/login/login-response.interface';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthService {

	constructor(private authe: AngularFireAuth) {
		console.log('Hello Auth Provider');
	}

	getAuthenticatedUser() {
		return this.authe.authState;
	}

	async createUserWithEmailAndPassword(account: Account) {
		try {
			return <LoginResponse>{
				result: await this.authe.auth.createUserWithEmailAndPassword(account.email, account.password)
			}
		}
		catch (e) {
			return <LoginResponse>{
				error: e
			};
		}
	}

	async signInWithEmailAndPassword(account: Account) {
		try {
			return <LoginResponse>{
				result: await this.authe.auth.signInWithEmailAndPassword(account.email, account.password)
			};
		}
		catch (e) {
			return <LoginResponse>{
				error: e
			};
		}
	}

	signOut() {
		this.authe.auth.signOut();
	}

}
